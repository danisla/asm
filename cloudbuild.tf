###################### CLOUD BUILD #####################
# To deploy the application in your Kubernetes cluster, Cloud Build needs the Kubernetes Engine Developer IAM Role
# Add IAM container.developer role to the ops proj Cloudbuild SA in the ops, dev1 and dev2 projects

# Check Cloudbuild SA is created
resource "null_resource" "exec_check_for_cloudbuild_service_accounts" {
  provisioner "local-exec" {
    command = <<EOT
      for (( c=1; c<=40; c++))
        do
          CHECK=`gcloud projects get-iam-policy ${module.create_ops_asm_project.project_number} --format=json | jq '.bindings[]' | jq -r '. | select(.role == "roles/container.serviceAgent").members[]'`

          if [[ "$CHECK" ]]; then
            echo "Cloud Build service account created."
            break;
          fi

          echo "Waiting for Cloud Build service account to be created."
          sleep 2
        done
    EOT

    interpreter = ["/bin/bash", "-c"]
  }
}

# Add IAM container.developer role to the ops proj Cloudbuild SA in the ops project
resource "google_project_iam_binding" "ops_cloudbuild_sa_ops" {
  project = module.create_ops_asm_project.project_id
  role    = "roles/container.admin"
  members = [
      "serviceAccount:${module.create_ops_asm_project.project_number}@cloudbuild.gserviceaccount.com",
  ]
  depends_on = [
    null_resource.exec_check_for_cloudbuild_service_accounts
  ]

}

# Add IAM container.developer role to the ops proj Cloudbuild SA in the dev1 project
resource "google_project_iam_binding" "ops_cloudbuild_sa_dev1" {
  project = module.create_dev1_project.project_id
  role    = "roles/container.admin"
  members = [
      "serviceAccount:${module.create_ops_asm_project.project_number}@cloudbuild.gserviceaccount.com",
  ]
  depends_on = [
    null_resource.exec_check_for_cloudbuild_service_accounts
  ]
}

# Add IAM container.developer role to the ops proj Cloudbuild SA in the dev2 project
resource "google_project_iam_binding" "ops_cloudbuild_sa_dev2" {
  project = module.create_dev2_project.project_id
  role    = "roles/container.admin"
  members = [
      "serviceAccount:${module.create_ops_asm_project.project_number}@cloudbuild.gserviceaccount.com",
  ]
  depends_on = [
    null_resource.exec_check_for_cloudbuild_service_accounts
  ]
}

# Add a cloudbuild trigger for k8s manifests
resource "google_cloudbuild_trigger" "k8s_trigger" {
  project = module.create_ops_asm_project.project_id
  trigger_template {
    branch_name = "master"
    repo_name   = google_sourcerepo_repository.k8s_repo.name
  }

  filename = "cloudbuild.yaml"
}

# Create a cloudbuild.yaml file
resource "null_resource" "exec_make_cloudbuild_yaml" {
  provisioner "local-exec" {
    command = <<EOT
    cat > cloudbuild.yaml <<EOF
steps:
# This step deploys the manifests to the gke-asm-1-r1-prod cluster
- name: 'gcr.io/cloud-builders/kubectl'
  id: Deploy-ops-asm-1
  args:
  - 'apply'
  - '-f'
  - './gke-asm-1-r1-prod'
  env:
  - 'CLOUDSDK_CORE_PROJECT=${module.create_ops_asm_project.project_id}'
  - 'CLOUDSDK_COMPUTE_REGION=${module.create_gke_1_ops_asm_subnet_01.location}'
  - 'CLOUDSDK_CONTAINER_CLUSTER=${module.create_gke_1_ops_asm_subnet_01.name}'

# This step deploys the manifests to the gke-asm-2-r2-prod cluster
- name: 'gcr.io/cloud-builders/kubectl'
  id: Deploy-ops-asm-2
  args:
  - 'apply'
  - '-f'
  - './gke-asm-2-r2-prod'
  env:
  - 'CLOUDSDK_CORE_PROJECT=${module.create_ops_asm_project.project_id}'
  - 'CLOUDSDK_COMPUTE_REGION=${module.create_gke_2_ops_asm_subnet_02.location}'
  - 'CLOUDSDK_CONTAINER_CLUSTER=${module.create_gke_2_ops_asm_subnet_02.name}'

# This step deploys the manifests to the gke-1-apps-r1a-prod cluster
- name: 'gcr.io/cloud-builders/kubectl'
  id: Deploy-gke-1-apps-r1a-prod
  args:
  - 'apply'
  - '-f'
  - './gke-1-apps-r1a-prod'
  env:
  - 'CLOUDSDK_CORE_PROJECT=${module.create_dev1_project.project_id}'
  - 'CLOUDSDK_COMPUTE_REGION=${module.create_gke_1_dev1_r1a_subnet_03.location}'
  - 'CLOUDSDK_CONTAINER_CLUSTER=${module.create_gke_1_dev1_r1a_subnet_03.name}'

# This step deploys the manifests to the gke-2-apps-r1b-prod cluster
- name: 'gcr.io/cloud-builders/kubectl'
  id: Deploy-gke-2-apps-r1b-prod
  args:
  - 'apply'
  - '-f'
  - './gke-2-apps-r1b-prod'
  env:
  - 'CLOUDSDK_CORE_PROJECT=${module.create_dev1_project.project_id}'
  - 'CLOUDSDK_COMPUTE_REGION=${module.create_gke_2_dev1_r1b_subnet_03.location}'
  - 'CLOUDSDK_CONTAINER_CLUSTER=${module.create_gke_2_dev1_r1b_subnet_03.name}'

# This step deploys the manifests to the gke-3-apps-r2a-prod cluster
- name: 'gcr.io/cloud-builders/kubectl'
  id: Deploy-gke-3-apps-r2a-prod
  args:
  - 'apply'
  - '-f'
  - './gke-3-apps-r2a-prod'
  env:
  - 'CLOUDSDK_CORE_PROJECT=${module.create_dev2_project.project_id}'
  - 'CLOUDSDK_COMPUTE_REGION=${module.create_gke_3_dev2_r2a_subnet_04.location}'
  - 'CLOUDSDK_CONTAINER_CLUSTER=${module.create_gke_3_dev2_r2a_subnet_04.name}'

# This step deploys the manifests to the gke-4-apps-r2b-prod cluster
- name: 'gcr.io/cloud-builders/kubectl'
  id: Deploy-gke-4-apps-r2b-prod
  args:
  - 'apply'
  - '-f'
  - './gke-4-apps-r2b-prod'
  env:
  - 'CLOUDSDK_CORE_PROJECT=${module.create_dev2_project.project_id}'
  - 'CLOUDSDK_COMPUTE_REGION=${module.create_gke_4_dev2_r2b_subnet_04.location}'
  - 'CLOUDSDK_CONTAINER_CLUSTER=${module.create_gke_4_dev2_r2b_subnet_04.name}'
    EOT
  }
}