# project ids
    output "host_project_id" { value = "${module.create_host_project.project_id}" }
    output "ops_project_id" { value = "${module.create_ops_asm_project.project_id}" }
    output "dev1_project_id" { value = "${module.create_dev1_project.project_id}" }
    output "dev2_project_id" { value = "${module.create_dev2_project.project_id}" }