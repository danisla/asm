###################  GKE CLUSTERS  #######################
# gke-asm-1-r1-prod - Create GKE regional cluster in ops-asm project using subnet-01
module "create_gke_1_ops_asm_subnet_01" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "5.1.1"
  
  project_id                 = module.create_ops_asm_project.project_id
  name                       = var.gke_asm_r1
  kubernetes_version         = var.kubernetes_version
  region                     = var.subnet_01_region
  zones                      = ["${var.subnet_01_region}-a", "${var.subnet_01_region}-b", "${var.subnet_01_region}-c"]
  network_project_id         = module.create_vpc_in_host_project.svpc_host_project_id
  network                    = module.create_vpc_in_host_project.network_name
  subnetwork                 = var.subnet_01_name
  ip_range_pods              = var.subnet_01_secondary_pod_name
  ip_range_services          = var.subnet_01_secondary_svc_1_name

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 6
      max_count          = 10
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# gke-asm-2-r2-prod - Create GKE regional cluster in ops-asm project using subnet-02
module "create_gke_2_ops_asm_subnet_02" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "5.1.1"
  
  project_id                 = module.create_ops_asm_project.project_id
  name                       = var.gke_asm_r2
  kubernetes_version         = var.kubernetes_version
  region                     = var.subnet_02_region
  zones                      = ["${var.subnet_02_region}-a", "${var.subnet_02_region}-b", "${var.subnet_02_region}-c"]
  network_project_id         = module.create_vpc_in_host_project.svpc_host_project_id
  network                    = module.create_vpc_in_host_project.network_name
  subnetwork                 = var.subnet_02_name
  ip_range_pods              = var.subnet_02_secondary_pod_name
  ip_range_services          = var.subnet_02_secondary_svc_1_name

    node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 6
      max_count          = 10
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}


# gke-1-apps-r1a-prod - Create GKE zonal cluster in dev1 project using subnet-03 zone a
module "create_gke_1_dev1_r1a_subnet_03" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "5.1.1"
  
  project_id                 = module.create_dev1_project.project_id
  name                       = var.gke_dev1-r1a
  kubernetes_version         = var.kubernetes_version
  region                     = var.subnet_03_region
  regional                   = false
  zones                      = ["${var.subnet_03_region}-a"]
  network_project_id         = module.create_vpc_in_host_project.svpc_host_project_id
  network                    = module.create_vpc_in_host_project.network_name
  subnetwork                 = var.subnet_03_name
  ip_range_pods              = var.subnet_03_secondary_pod_name
  ip_range_services          = var.subnet_03_secondary_svc_1_name

    node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 3
      max_count          = 10
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}


# gke-2-apps-r1b-prod - Create GKE zonal cluster in dev1 project using subnet-03 zone b
module "create_gke_2_dev1_r1b_subnet_03" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "5.1.1"
  
  project_id                 = module.create_dev1_project.project_id
  name                       = var.gke_dev1-r1b
  kubernetes_version         = var.kubernetes_version
  region                     = var.subnet_03_region
  regional                   = false
  zones                      = ["${var.subnet_03_region}-b"]
  network_project_id         = module.create_vpc_in_host_project.svpc_host_project_id
  network                    = module.create_vpc_in_host_project.network_name
  subnetwork                 = var.subnet_03_name
  ip_range_pods              = var.subnet_03_secondary_pod_name
  ip_range_services          = var.subnet_03_secondary_svc_2_name

    node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 3
      max_count          = 10
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# gke-3-apps-r2a-prod - Create GKE zonal cluster in dev2 project using subnet-04 zone a
module "create_gke_3_dev2_r2a_subnet_04" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "5.1.1"
  
  project_id                 = module.create_dev2_project.project_id
  name                       = var.gke_dev2-r2a
  kubernetes_version         = var.kubernetes_version
  region                     = var.subnet_04_region
  regional                   = false
  zones                      = ["${var.subnet_04_region}-a"]
  network_project_id         = module.create_vpc_in_host_project.svpc_host_project_id
  network                    = module.create_vpc_in_host_project.network_name
  subnetwork                 = var.subnet_04_name
  ip_range_pods              = var.subnet_04_secondary_pod_name
  ip_range_services          = var.subnet_04_secondary_svc_1_name

    node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 3
      max_count          = 10
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}


# gke-4-apps-r2b-prod - Create GKE zonal cluster in dev2 project using subnet-04 zone b
module "create_gke_4_dev2_r2b_subnet_04" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "5.1.1"
  
  project_id                 = module.create_dev2_project.project_id
  name                       = var.gke_dev2-r2b
  kubernetes_version         = var.kubernetes_version
  region                     = var.subnet_04_region
  regional                   = false
  zones                      = ["${var.subnet_04_region}-b"]
  network_project_id         = module.create_vpc_in_host_project.svpc_host_project_id
  network                    = module.create_vpc_in_host_project.network_name
  subnetwork                 = var.subnet_04_name
  ip_range_pods              = var.subnet_04_secondary_pod_name
  ip_range_services          = var.subnet_04_secondary_svc_2_name

    node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 3
      max_count          = 10
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# Grant Compute Security Admin IAM role to the Kubernetes SA in the network host project
resource "google_project_iam_binding" "ops_dev1_dev2_gke_sa_security_admin_in_host" {
  project = module.create_host_project.project_id
  role    = "roles/compute.securityAdmin"
  members = [
      "serviceAccount:service-${module.create_ops_asm_project.project_number}@container-engine-robot.iam.gserviceaccount.com",
      "serviceAccount:service-${module.create_dev1_project.project_number}@container-engine-robot.iam.gserviceaccount.com",
      "serviceAccount:service-${module.create_dev2_project.project_number}@container-engine-robot.iam.gserviceaccount.com",
  ]
  depends_on = [
    null_resource.exec_create_kubeconfig,
    null_resource.exec_check_for_gke_service_accounts
  ]
}

resource "null_resource" "exec_check_for_gke_service_accounts" {
  provisioner "local-exec" {
    command = <<EOT
      for (( c=1; c<=40; c++))
        do
          CHECK1=`gcloud projects get-iam-policy ${module.create_ops_asm_project.project_number} --format=json | jq '.bindings[]' | jq -r '. | select(.role == "roles/container.serviceAgent").members[]'`
          CHECK2=`gcloud projects get-iam-policy ${module.create_dev1_project.project_number} --format=json | jq '.bindings[]' | jq -r '. | select(.role == "roles/container.serviceAgent").members[]'`
          CHECK3=`gcloud projects get-iam-policy ${module.create_dev2_project.project_number} --format=json | jq '.bindings[]' | jq -r '. | select(.role == "roles/container.serviceAgent").members[]'`

          if [[ "$CHECK1" ]] && [[ "$CHECK2" ]] && [[ "$CHECK3" ]]; then
            echo "GKE service accounts created."
            break;
          fi

          echo "Waiting for GKE service accounts to be created."
          sleep 2
        done
    EOT

    interpreter = ["/bin/bash", "-c"]
  }
}


# Create a new kubeconfig file for the six clusters
# The kubeconfig file is called kubemesh and is placed in the local folder
# Set your KUBECONFIG variable to "kubemesh" to access the clusters
resource "null_resource" "exec_create_kubeconfig" {
  provisioner "local-exec" {
    command = <<EOT
    gcloud beta container clusters get-credentials "${module.create_gke_1_ops_asm_subnet_01.name}" --region "${module.create_gke_1_ops_asm_subnet_01.region}" --project "${module.create_ops_asm_project.project_id}"
    gcloud beta container clusters get-credentials "${module.create_gke_2_ops_asm_subnet_02.name}" --region "${module.create_gke_2_ops_asm_subnet_02.region}" --project "${module.create_ops_asm_project.project_id}"
    gcloud container clusters get-credentials "${module.create_gke_1_dev1_r1a_subnet_03.name}" --zone "${var.subnet_03_region}-a" --project "${module.create_dev1_project.project_id}"
    gcloud container clusters get-credentials "${module.create_gke_2_dev1_r1b_subnet_03.name}" --zone "${var.subnet_03_region}-b" --project "${module.create_dev1_project.project_id}"
    gcloud container clusters get-credentials "${module.create_gke_3_dev2_r2a_subnet_04.name}" --zone "${var.subnet_04_region}-a" --project "${module.create_dev2_project.project_id}"
    gcloud container clusters get-credentials "${module.create_gke_4_dev2_r2b_subnet_04.name}" --zone "${var.subnet_04_region}-b" --project "${module.create_dev2_project.project_id}"
    kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context gke_"${module.create_ops_asm_project.project_id}"_"${module.create_gke_1_ops_asm_subnet_01.region}"_"${module.create_gke_1_ops_asm_subnet_01.name}" 
    kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context gke_"${module.create_ops_asm_project.project_id}"_"${module.create_gke_2_ops_asm_subnet_02.region}"_"${module.create_gke_2_ops_asm_subnet_02.name}" 
    kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context gke_"${module.create_dev1_project.project_id}"_"${var.subnet_03_region}-a"_"${module.create_gke_1_dev1_r1a_subnet_03.name}" 
    kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context gke_"${module.create_dev1_project.project_id}"_"${var.subnet_03_region}-b"_"${module.create_gke_2_dev1_r1b_subnet_03.name}" 
    kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context gke_"${module.create_dev2_project.project_id}"_"${var.subnet_04_region}-a"_"${module.create_gke_3_dev2_r2a_subnet_04.name}" 
    kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account) --context gke_"${module.create_dev2_project.project_id}"_"${var.subnet_04_region}-b"_"${module.create_gke_4_dev2_r2b_subnet_04.name}" 
    EOT

    environment = {
        KUBECONFIG = "kubemesh"
    }
  }
  depends_on = [
    module.create_gke_1_ops_asm_subnet_01,
    module.create_gke_2_ops_asm_subnet_02,
    module.create_gke_1_dev1_r1a_subnet_03,
    module.create_gke_2_dev1_r1b_subnet_03,
    module.create_gke_3_dev2_r2a_subnet_04,
    module.create_gke_4_dev2_r2b_subnet_04,
  ]
}
